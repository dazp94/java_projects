package pt.everis.converter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UTFtoUnicode {

	public static void main(String[] args) {

//		String fileIn  = args[0];
//		String fileOut = args[1];
		String fileIn  = "C:\\Users\\dsantopa\\Desktop\\i18n_client_dev_utf";
		String fileOut = "C:\\Users\\dsantopa\\Desktop\\i18n_client_dev_uni_cleaned";
		File folderIn = new File(fileIn);
		
		File[] listOfFiles = folderIn.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile()) {
		    	System.out.println("Converting: " + file.getAbsolutePath());
		        convertFile(file, fileOut);
		    }
		}
	}
	
	private static void convertFile(File fileEntry, String folderOut) {
		try(BufferedReader br= new BufferedReader(new FileReader(fileEntry));
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(folderOut+"\\"+fileEntry.getName())))
				) {

			String line;
			while((line=br.readLine())!=null){
				line = utfToUnicode(line);
				bw.write(line);
				bw.newLine();
			}
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}
	
	private static String utfToUnicode(String s) {
		String patternString = ".*([�������&��ꩀ�����]).*";
		String conv = "";

		Pattern pattern = Pattern.compile(patternString);

		Matcher matcher = pattern.matcher(s);
		boolean matches = matcher.matches();
		if(!matches) {
			return s;
		}
		else {
			String uc = matcher.group(1);
			conv = convertAndWrite(uc);
			return utfToUnicode(s.replaceAll(uc, conv));
		}
	}

	private static String convertAndWrite(String line){

		String result = "";
		char convert = line.charAt(0);
		int code = (int) convert;
		result = "\\\\u" + String.format("%04X",(code & 0xFFFF)).toUpperCase();
		return result;
	}
}
