import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpecialCharactersFinder {

	public static String folder  = "C:\\Users\\dsantopa\\Desktop\\i18n_client_dev_utf";


	public static void main(String[] args) {


		File folderIn = new File(folder);

		File[] listOfFiles = folderIn.listFiles();		Set<String> set = new HashSet<>();

		for (File file : listOfFiles) {
			if (file.isFile()) {
				System.out.println("Finding: " + file.getAbsolutePath());
				findInFile(file, set);
			}
		}
		System.out.println(set);
	}

	private static void findInFile(File fileEntry, Set set) {

		try(BufferedReader br= new BufferedReader(new FileReader(fileEntry))) {
			String line;
			while((line=br.readLine())!=null){
				unicodeToUTF(line, set);
			}
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}

	private static void unicodeToUTF(String s , Set set) {

		Pattern p = Pattern.compile("[^a-zA-Z0-9 ]");
		Matcher m = p.matcher(s);
		boolean b = m.find();

		//System.out.println("Something doesn't match !");
		if (b) {
			char[] chars = s.toCharArray();
			for (int i = 0; i < chars.length; i++) {
				String charedString = chars[i] + "";
				if (charedString.substring(0).matches("[^A-Za-z0-9 ]")){
					set.add(charedString);
				}
			}
		}
	}
}