package pt.everis.converter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UnicodeToUTF{

	public static void main(String[] args) {

		String fileIn  = "C:\\Users\\dsantopa\\Desktop\\i18n_client_dev";
		String fileOut = "C:\\Users\\dsantopa\\Desktop\\i18n_client_dev_utf";
		
		File folderIn = new File(fileIn);
		
		File[] listOfFiles = folderIn.listFiles();

		for (File file : listOfFiles) {
		    if (file.isFile()) {
		    	System.out.println("Converting: " + file.getAbsolutePath());
		        convertFile(file, fileOut);
		    }
		}
	}

	private static void convertFile(File fileEntry, String folderOut) {
		
		try(BufferedReader br= new BufferedReader(new FileReader(fileEntry));
				BufferedWriter bw = new BufferedWriter(new FileWriter(new File(folderOut+"\\"+fileEntry.getName())))
				) {

			String line;
			while((line=br.readLine())!=null){
				line=line.replaceAll("\\\\[u]000d","!_!_!");
				line=unicodeToUTF(line);
				line=line.replaceAll("!_!_!","\\\\u000d");
				bw.write(line);
				bw.newLine();
			}
		} catch (Exception e) {
			System.err.println("Error: " + e);
		}
	}


	private static String unicodeToUTF(String s) {
		String patternString = ".*(\\\\[u][0-9a-fA-F]{4}).*";
		String conv = "";

		Pattern pattern = Pattern.compile(patternString);

		Matcher matcher = pattern.matcher(s);
		boolean matches = matcher.matches();
		if(!matches) {
			return s;
		}
		else {
			String uc = "\\"+matcher.group(1);
			conv = convertAndWrite(uc);
			System.out.println(s.replaceAll(uc, conv));
			return unicodeToUTF(s.replaceAll(uc, conv));
		}
	}

	private static String convertAndWrite(String line){
		String str = line.split(" ")[0];

		str = str.replace("\\","");
		String[] arr = str.split("u");
		String text = "";

		for(int i = 1; i < arr.length; i++){
			int hexVal = Integer.parseInt(arr[i], 16);
			text += (char)hexVal;
		}
		return text;
	}



}
